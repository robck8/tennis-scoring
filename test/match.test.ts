import { Match } from "../src/match";

const PLAYER_1 = "player 1";
const PLAYER_2 = "player 2";

test("expect init score to be 0-0", () => {

    // Arrange 
    const match = new Match(PLAYER_1, PLAYER_2);

    // Act 
    const score = match.score();

    // Assert 
    expect(score).toBe("0-0");
});

test("expect set score when incomplete", () => {

    // Arrange 
    const match = new Match(PLAYER_1, PLAYER_2);
    match.pointWonBy(PLAYER_1);

    // Act 
    const score = match.score();

    // Assert 
    expect(score).toBe("0-0, 15-0");
});

test("expect match won after 1st set", () => {

    // Arrange 
    const match = new Match(PLAYER_1, PLAYER_2);

    const pointsToWinMatch = 4 * 6;
    for (let i = 0; i < pointsToWinMatch; i++) {
        match.pointWonBy(PLAYER_2);
    }

    // Act 
    const score = match.score();

    // Assert 
    expect(score).toBe("Match won by player 2");
});
