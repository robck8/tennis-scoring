import { BasicGameState } from "../../src/game-state-machine/basic-game-state";
import { FinishedGameState } from "../../src/game-state-machine/finished-game-state";

const PLAYER_1 = "player 1";
const PLAYER_2 = "player 2";
const SCORE_MAP = [0, 15, 30, 40];

test("finished game returned", () => {

    // Arrange 
    let game = new BasicGameState({
        player1: PLAYER_1,
        player2: PLAYER_2,
        scoreMap: SCORE_MAP,
        pointsWon: []
    });

    // Act 
    for (let i = 0; i < 4; i++) {
        game = game.pointWonBy(PLAYER_1);
    }

    // Assert 
    expect(game).toBeInstanceOf(FinishedGameState);
});