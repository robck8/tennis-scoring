import { Game } from "../src/game";

const PLAYER_1 = "player 1";
const PLAYER_2 = "player 2";

// Could move these tests to each state machine.

test.each([
    [0, 0, "0-0"],
    [0, 1, "0-15"],
    [0, 3, "0-40"],
    [1, 0, "15-0"],
    [1, 3, "15-40"],
    [2, 3, "30-40"],
    [3, 3, "deuce"],
])("expect under 4 points to match score", (player1Points: number, player2Points: number, expectedScore: string) => {

    // Arrange 
    const game = getGameWithScore(player1Points, player2Points);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe(expectedScore);
});

test("expect advantange player 1", () => {

    // Arrange 
    const game = getGameWithScore(3, 3);
    game.pointWonBy(PLAYER_1);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe("advantage player 1");
});


test("expect advantange player 2", () => {

    // Arrange 
    const game = getGameWithScore(3, 3);
    game.pointWonBy(PLAYER_2);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe("advantage player 2");
});

test("expect deuce after advantage", () => {

    // Arrange 
    const game = getGameWithScore(3, 3);
    game.pointWonBy(PLAYER_1);
    game.pointWonBy(PLAYER_2);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe("deuce");
});


test("expect advantage player 1 after deuce", () => {

    // Arrange 
    const game = getGameWithScore(3, 3);
    game.pointWonBy(PLAYER_1);
    game.pointWonBy(PLAYER_2);
    game.pointWonBy(PLAYER_1);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe("advantage player 1");
});


test("expect advantage player 2 after deuce", () => {

    // Arrange 
    const game = getGameWithScore(3, 3);
    game.pointWonBy(PLAYER_1);
    game.pointWonBy(PLAYER_2);
    game.pointWonBy(PLAYER_2);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe("advantage player 2");
});

function getGameWithScore(player1Points: number, player2Points: number): Game {

    if (player1Points > 3 || player2Points > 3) {
        throw new Error("getGameWithScore not supported for scores > 3");
    }

    const game = new Game(PLAYER_1, PLAYER_2);

    for (let i = 0; i < player1Points; i++) {
        game.pointWonBy(PLAYER_1);
    }

    for (let i = 0; i < player2Points; i++) {
        game.pointWonBy(PLAYER_2);
    }

    return game;
}