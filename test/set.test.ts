
import { Set } from "../src/set";

const PLAYER_1 = "player 1";
const PLAYER_2 = "player 2";
const POINTS_TO_WIN_GAME = 4;

test.each([
    [0, 0, "0-0"],
    [0, 1, "0-1"],
    [1, 0, "1-0"],
    [1, 3, "1-3"],
    [2, 3, "2-3"],
    [3, 3, "3-3"],
    [3, 5, "3-5"],
    [4, 5, "4-5"],
    [5, 2, "5-2"],
    [5, 5, "5-5"],
])("expect basic set score", (player1GamesWon: number, player2GamesWon: number, expectedScore: string) => {

    // Arrange 
    const set = buildSetWithScoreInSeries(player1GamesWon, player2GamesWon);

    // Act 
    const score = set.score();

    // Assert 
    expect(score).toBe(expectedScore);
});


test("includes game score when game incomplete", () => {

    // Arrange 
    const set = buildSetWithScoreAlternating(2, 3);
    set.pointWonBy(PLAYER_2);

    // Act 
    const score = set.score();
    const isTieBreak = set.isTieBreak();

    // Assert 
    expect(score).toBe("2-3, 0-15");
});


test.each([
    [6, 5, "6-5"],
    [5, 6, "5-6"],
])("expect 5-6 score to show (when alternating sets)", (player1GamesWon: number, player2GamesWon: number, expectedScore: string) => {

    // Arrange 
    const set = buildSetWithScoreAlternating(player1GamesWon, player2GamesWon);

    // Act 
    const score = set.score();

    // Assert 
    expect(score).toBe(expectedScore);
});

test.each([
    [6, 0, "Set won by player 1"],
    [6, 4, "Set won by player 1"],
    [3, 6, "Set won by player 2"],
    [4, 6, "Set won by player 2"],
])("set won when at least 6 games and at least 2 games more than the opponent", (player1GamesWon: number, player2GamesWon: number, expectedScore: string) => {

    // Arrange 
    const set = buildSetWithScoreAlternating(player1GamesWon, player2GamesWon);

    // Act 
    const score = set.score();

    // Assert 
    expect(score).toBe(expectedScore);
});

test.each([
    [7, 6, "Set won by player 1"],
    [6, 7, "Set won by player 2"],
])("winner after tie break", (player1GamesWon: number, player2GamesWon: number, expectedScore: string) => {

    // Arrange 
    const set = buildSetWithScoreAlternating(player1GamesWon, player2GamesWon);

    // Act 
    const score = set.score();

    // Assert 
    expect(score).toBe(expectedScore);
});


test("expect tie break when 6-6", () => {

    // Arrange 
    const set = buildSetWithScoreAlternating(6, 6);

    // Act 
    const score = set.score();
    const isTieBreak = set.isTieBreak();

    // Assert 
    expect(score).toBe("tie break");
    expect(isTieBreak).toBe(true);
});


function buildSetWithScoreInSeries(player1GamesWon: number, player2GamesWon: number): Set {

    if (player1GamesWon >= 6 && player2GamesWon >= 6) {
        throw new Error("getSetWithScore not supported when both players games >= 6");
    }

    const set = new Set(PLAYER_1, PLAYER_2);


    for (let i = 0; i < player1GamesWon; i++) {
        for (let j = 0; j < POINTS_TO_WIN_GAME; j++) {
            set.pointWonBy(PLAYER_1);
        }
    }

    for (let i = 0; i < player2GamesWon; i++) {
        for (let j = 0; j < POINTS_TO_WIN_GAME; j++) {
            set.pointWonBy(PLAYER_2);
        }
    }

    return set;
}


function buildSetWithScoreAlternating(player1GamesWon: number, player2GamesWon: number): Set {

    const set = new Set(PLAYER_1, PLAYER_2);

    for (let i = 0; i < Math.max(player1GamesWon, player2GamesWon); i++) {

        if (i < player1GamesWon) {
            for (let j = 0; j < POINTS_TO_WIN_GAME; j++) {
                set.pointWonBy(PLAYER_1);
            }
        }

        if (i < player2GamesWon) {
            for (let j = 0; j < POINTS_TO_WIN_GAME; j++) {
                set.pointWonBy(PLAYER_2);
            }
        }
    }

    return set;
}