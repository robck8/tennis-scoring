import { TieBreakGame } from "../src/tie-break-game";

const PLAYER_1 = "player 1";
const PLAYER_2 = "player 2";

test("expect new game score to be 0-0", () => {

    // Arrange 
    const game = new TieBreakGame(PLAYER_1, PLAYER_2);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe("0-0");
});


test.each([
    [0, 0, "0-0"],
    [0, 1, "0-1"],
    [0, 3, "0-3"],
    [1, 0, "1-0"],
    [1, 3, "1-3"],
    [2, 3, "2-3"],
    [3, 3, "deuce"],
])("expect under 4 points to match score", (player1Points: number, player2Points: number, expectedScore: string) => {

    // Arrange 
    const game = getGameWithScore(player1Points, player2Points);

    // Act 
    const score = game.score();

    // Assert 
    expect(score).toBe(expectedScore);
});


function getGameWithScore(player1Points: number, player2Points: number): TieBreakGame {

    if (player1Points > 3 || player2Points > 3) {
        throw new Error("getGameWithScore not supported for points > 3");
    }

    const game = new TieBreakGame(PLAYER_1, PLAYER_2);

    for (let i = 0; i < player1Points; i++) {
        game.pointWonBy(PLAYER_1);
    }

    for (let i = 0; i < player2Points; i++) {
        game.pointWonBy(PLAYER_2);
    }

    return game;
}