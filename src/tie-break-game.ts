import { Game } from "./game";

export class TieBreakGame extends Game {

    constructor(_player1: string, _player2: string, _scoreMap = [0, 1, 2, 3]) {
        super(_player1, _player2, _scoreMap);
    }
}
