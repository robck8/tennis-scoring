import { SetState, SetContext } from "./set-state";
import { FinishedSetState } from "./finished-set-state";

export class TieBreakSetState extends SetState {

    constructor(context: SetContext) {
        super(context);
    }

    pointWonBy(player: string): SetState {
        super.pointWonBy(player);
        if (this.currentGame().isComplete()) {
            return new FinishedSetState(player, this.context);
        }

        return this;
    }

    score(): string {
        const currentGame = this.currentGame();
        if (!currentGame.hasStarted()) {
            return "tie break";
        }

        return `tie break, ${currentGame.score()}`;
    }
}