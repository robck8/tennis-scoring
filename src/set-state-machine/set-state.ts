import { Game } from "../game";
import { TieBreakGame } from "../tie-break-game";

export interface SetContext {
    player1: string;
    player2: string;
    games: Game[],
}

export abstract class SetState {

    constructor(protected context: SetContext) {

    }

    protected currentGame(): Game {
        return this.context.games[this.context.games.length - 1];
    }

    isTieBreak(): boolean {
        return this.currentGame() instanceof TieBreakGame;
    }

    pointWonBy(player: string): SetState {
        this.currentGame().pointWonBy(player);
        return this;
    }

    abstract score(): string;

    getGamePoints(): { player1: number; player2: number } {
        const winners = this.context.games.map(x => x.winner());

        let player1 = 0;
        let player2 = 0;
        for (const winner of winners) {
            if (winner === null) {
                break;
            }

            if (winner == this.context.player1) {
                player1++;
            } else {
                player2++;
            }
        }

        return {
            player1,
            player2,
        };
    }
}