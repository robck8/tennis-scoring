import { SetState, SetContext } from "./set-state";

export class FinishedSetState extends SetState {

    constructor(private _winner: string, context: SetContext) {
        super(context);
    }

    pointWonBy(player: string): SetState {
        return this;
    }

    score(): string {
        return `Set won by ${this._winner}`;
    }

    winner(): string {
        return this._winner;
    }
}