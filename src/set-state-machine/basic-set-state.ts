import { SetState, SetContext } from "./set-state";
import { FinishedSetState } from "./finished-set-state";
import { TieBreakGame } from "../tie-break-game";
import { Game } from "../game";
import { TieBreakSetState } from "./tie-break-set-state";

export class BasicSetState extends SetState {

    constructor(context: SetContext) {
        super(context);
    }

    pointWonBy(player: string): SetState {
        super.pointWonBy(player);

        const currentGame = this.currentGame();
        if (currentGame.isComplete()) {
            const points = this.getGamePoints();

            const minWinPoints = 6;
            if (points.player1 >= minWinPoints || points.player2 >= minWinPoints) {
                const diff = points.player1 - points.player2;

                if (diff >= 2) {
                    return new FinishedSetState(this.context.player1, this.context);
                } else if (diff <= -2) {
                    return new FinishedSetState(this.context.player2, this.context);
                }

                if (points.player1 === minWinPoints && points.player2 === minWinPoints) {
                    this.context.games.push(new TieBreakGame(this.context.player1, this.context.player2));
                    return new TieBreakSetState(this.context);
                }
            }

            this.context.games.push(new Game(this.context.player1, this.context.player2));
        }

        return this;
    }

    score(): string {
        const points = this.getGamePoints();

        const currentGame = this.currentGame();

        if (currentGame.isComplete() || !currentGame.hasStarted()) {
            return `${points.player1}-${points.player2}`;
        }

        return `${points.player1}-${points.player2}, ${currentGame.score()}`;
    }
}