import { Set } from "./set";

export class Match {

    private _set: Set;

    constructor(private _player1: string, private _player2: string) {
        this._set = new Set(_player1, _player2);
    }

    public pointWonBy(player: string): void {
        this._set.pointWonBy(player);
    }

    public score(): string {
        const winner = this._set.winner();
        if (winner) {
            return `Match won by ${winner}`;
        }

        return this._set.score();
    }
}