import { GameState, GameContext } from "./game-state";

export class FinishedGameState extends GameState {

    constructor(
        private _winner: string,
        context: GameContext,
    ) {
        super(context);
    }

    pointWonBy(player: string): GameState {
        return this;
    }

    score(): string {
        return `Game ${this._winner}`;
    }

    winner(): string {
        return this._winner;
    }
}