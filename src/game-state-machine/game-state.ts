export interface GameContext {
    player1: string;
    player2: string;
    pointsWon: string[],
    scoreMap: number[];
}

export abstract class GameState {

    constructor(protected context: GameContext) { }

    pointWonBy(player: string): GameState {
        this.context.pointsWon.push(player);
        return this;
    }

    abstract score(): string;

    mapPointsToScore(points: number): number {
        return this.context.scoreMap[points];
    }

    hasStarted(): boolean {
        return this.context.pointsWon.length > 0;
    }

    getPlayerPoints(): { player1: number; player2: number } {

        let player1 = 0;
        let player2 = 0;
        for (const point of this.context.pointsWon) {
            if (point == this.context.player1) {
                player1++;
            } else {
                player2++;
            }
        }

        return {
            player1,
            player2,
        };
    }
} 