import { GameState, GameContext } from "./game-state";
import { AdvantageGameState } from "./advantage-game-state";

export class DeuceGameState extends GameState {

    constructor(context: GameContext) {
        super(context);
    }

    pointWonBy(player: string): GameState {
        super.pointWonBy(player);

        return new AdvantageGameState(player, this.context);
    }

    score(): string {
        return "deuce";
    }
}