import { GameState, GameContext } from "./game-state";
import { FinishedGameState } from "./finished-game-state";
import { DeuceGameState } from "./deuce-game-state";

export class AdvantageGameState extends GameState {

    constructor(
        private _advantagePlayer: string,
        context: GameContext,
    ) {
        super(context);
    }

    pointWonBy(player: string): GameState {
        super.pointWonBy(player);

        if (player == this._advantagePlayer) {
            return new FinishedGameState(player, this.context);
        }

        return new DeuceGameState(this.context);
    }

    score(): string {
        return `advantage ${this._advantagePlayer}`;
    }
}