import { GameState, GameContext } from "./game-state";
import { FinishedGameState } from "./finished-game-state";
import { DeuceGameState } from "./deuce-game-state";

export class BasicGameState extends GameState {

    constructor(context: GameContext) {
        super(context);
    }

    pointWonBy(player: string): GameState {
        super.pointWonBy(player);

        const points = this.getPlayerPoints();

        const minWinPoints = 4;
        if (points.player1 >= minWinPoints || points.player2 >= minWinPoints) {
            const diff = points.player1 - points.player2;

            if (diff >= 2) {
                return new FinishedGameState(this.context.player1, this.context);
            } else if (diff <= -2) {
                return new FinishedGameState(this.context.player2, this.context);
            }
        }

        if (points.player1 === 3 && points.player2 === 3) {
            return new DeuceGameState(this.context);
        }

        return this;
    }

    score(): string {
        const points = super.getPlayerPoints();

        return `${this.mapPointsToScore(points.player1)}-${this.mapPointsToScore(points.player2)}`;
    }
}