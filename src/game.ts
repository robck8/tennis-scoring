import { GameState } from "./game-state-machine/game-state";
import { BasicGameState } from "./game-state-machine/basic-game-state";
import { FinishedGameState } from "./game-state-machine/finished-game-state";

export class Game {

    private _state: GameState;

    constructor(player1: string, player2: string, scoreMap = [0, 15, 30, 40]) {
        if (scoreMap.length < 4) throw new Error("_scoreMap length must be greater than 4");

        this._state = new BasicGameState({
            player1,
            player2,
            scoreMap,
            pointsWon: []
        })
    }

    pointWonBy(player: string): void {
        this._state = this._state.pointWonBy(player);
    }

    hasStarted(): boolean {
        return this._state.hasStarted();
    }

    isComplete(): boolean {
        return this._state instanceof FinishedGameState;
    }

    winner(): string | null {
        if (this._state instanceof FinishedGameState) {
            return this._state.winner();
        }

        return null;
    }

    score(): string {
        return this._state.score();
    }
}
