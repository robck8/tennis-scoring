import { Game } from "./game";
import { TieBreakGame } from "./tie-break-game";
import { BasicSetState } from "./set-state-machine/basic-set-state";
import { SetState } from "./set-state-machine/set-state";
import { FinishedSetState } from "./set-state-machine/finished-set-state";

export class Set {

    private _setState: SetState;

    constructor(player1: string, player2: string) {
        this._setState = new BasicSetState({
            player1,
            player2,
            games: [new Game(player1, player2)],
        })
    }

    public pointWonBy(player: string): void {
        this._setState = this._setState.pointWonBy(player);
    }

    isTieBreak(): boolean {
        return this._setState.isTieBreak();
    }

    winner(): string | null {
        if (this._setState instanceof FinishedSetState) {
            return this._setState.winner();
        }

        return null;
    }

    score(): string {
        return this._setState.score();
    }
}